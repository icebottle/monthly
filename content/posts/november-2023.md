+++
title = 'November 2023'
date = 2023-11-28T09:11:51+05:30
draft = false
+++

# Tried a Refurbished Business Laptop
I got my hands on a refurbished business laptop from an outlet near my city of work. It is a 14 inch dell laptop running 6th generation intel i5 with a memory of 8gb and an ssd storage of 256gb. Body looked clean and I didn’t see signs of use on keyboard or trackpad. The outlet offerred this laptop at a budget of ~23k.

Seller allowed me to check whether input and output components are working as expected and he gave me a usb drive with which I checked if usb ports are working. The laptop had a great viewing angle and its colors looked good. There was another smaller laptop at the same budget with a higher generation processor but it had a display that didn’t look good. The display looked even worse at an angle.

I got information from the seller that the laptop which I liked was imported from a foreign company where it was used for one year. I know from online resources that the processor generation was released in 2015. Seller informed me the manufacturing date of this piece was in or around 2019. He also assured me the windows os present in the laptop is genuine and said he gets genuine windows licences from a private agent. 

I came to know from windows update settings that the laptop’s hardware does not meet the minimum requirements for windows 11. There is a health check software that is linked to from the windows update settings which can scan the hardware and tell what conditions aren’t met. The laptop’s processor was unsupported and there was some unmet condition regarding TPM which I didn’t bother to read about.

# Scaled my Personal Drive
I rented and attached a data volume on my personal cloud server instance which I use for storing and managing data. I did this because the data volume can be scaled as I add more data into it. I rented another data volume for the purpose of storing backups and configured my backup process to use that volume. 

I configured a basic mechanism to take certain information and periodically write it into a file. I shared this file using my hosted cloud file manager and added an internet shortcut in my smartphone to access it. So now I can easily see how much space is available for me to store data and also see when was the last successful backup taken.

# Spawned a Nextcloud Instance
I spawned a nextcloud instance for personal use. I did this despite having another setup for storing files. There is two reasons why I wanted to try out nextcloud. One reason is that I have faced trouble uploading large files in my existing setup. Second reason is that I want to explore integrations offered by nextcloud on the different operating systems that my devices run on. 

The cloud provider from which I spawned the nextcloud instance offered it as an easy deployment option. It allowed me to choose from a set of available specifications for a virtual private server where the application will run. There was no additional charge for deploying a vps running nextcloud compared to deploying a general purpose vps. I chose the former option. It was convenient to set up. Another plus point is that my nextcloud instance runs in an environment where it is not competing for resources alongside other services which I run.

I chose to provision a storage volume and attach it to the vps instead of going with vps’s default storage. This lets me increase the storage space when required. The cloud provider claims I can increase the storage up to 1000 tb by paying more.