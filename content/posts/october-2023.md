+++
title = 'October 2023'
date = 2023-11-02T19:29:57+05:30
draft = false
+++

# Configured a Multi-device Synchronized Folder with Daily Backup
I created a folder in the server I rented and synchronized that folder with my two devices - mobile and laptop - by using syncthing. I configured borgbackup to run daily backups on the folder in the server. End result is twofold. Firstly, changes I make to the contents of the folder n one device will start synchronizing over the internet to the respective peer folder in the other device as well as in the server. Secondly, the contents of the folder in server will be incrementally backed up to another folder at a fixed time everyday.

I expect some risk of data loss, which can happen if the folder content get corrupted in one of the device, in which case it might synchronize the corruption to its peers. In that case the data can be recovered to previous day’s condition if the regular backups made in server is intact.

I also deployed a file browsing application on top of the server’s folder and exposed its web interface to the internet. This way I can manage the contents of the folder from devices I do not own by opening a web browser and entering my credentials to access the file browser web application.

Having configured the multi-device synchronized folder with web interface, I have the convenience of managing files from my laptop, mobile and even from the web browser of someone else’s laptop. This enables me to use my own storage options for private files instead of storing it in cloud storage providers that have privacy policies not to my liking.

# Upgraded Personal System to Ubuntu 23.04
My laptop was running Ubuntu 22.10 in which my attempts to update packages was not working. My friend told me this is because 22.10 is out of support and the sources to which my laptop points to can no longer be used. He gave me another source that keeps all the past releases and asked me to point to it and do an upgrade to 23.04. This worked and my laptop is now running 23.04.

I encountered a GUI defect which others have also reported for 23.04. Some area towards the top right corner of my screen was not responding properly to mouse clicks. This was related to a ghost user interface component being placed there capturing mouse clicks and hover events as understood from a bug report online. Discussions in the bug report also said the issue can be resolved by disabling Ubuntu App Indicator and following it with either a restart of gnome or a reboot of the system. I did as suggested and I do not face the issue now.

# Purchased a Wireless Neckband Headset
I find wireless headsets that come as a neck band more usable compared to category of small earbuds that is carried in its box or the category of large over the ear headphones. I don’t want to carry a small box in my pocket in addition to the other things that I already carry and I don’t want to wear a bulky headset either. I like the fact that neckband stays conveniently around the neck.

The earpieces are secured from falling to the ground in case of neckband, unlike standalone earbuds which fell off from my ear when I tried to wear my motorcycle helmet. Having headset inside helmet helps me get navigational aid from map application while driving. I can attach the smartphone to bike’s handle in order to see the map, but I switched to relying on audio navigational aids after purchasing this neckband because I didn’t want to keep my smartphone under direct sunlight and cause the device to heat up very often. 

One downside of this headset is that there is a chance the hanging earpieces will accidentally swing into a curry bowl while sitting down to eat.

In short a wireless neckband headset is low profile, convenient to carry, and can be used inside a helmet.