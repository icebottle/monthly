+++
title = 'September 2023'
date = 2023-09-22T22:17:07+05:30
draft = false
+++

# Explored Scents Created by a Perfumer
I have contact of a perfumer. I got it from a reddit post which listed popular perfumers in my country. Once I enquired about his perfumes through text message. Since then he used to share anatomies of perfumes occasionally. 

One day I found description of a perfume interesting and so I purchased its sample size decant from him along with another perfume which I had in my wishlist earlier. I received both of the perfumes and surprisingly I saw an extra perfume sample which I didn't pay for. I understood it was complementary after asking him about it. I liked each of the three perfumes. Sample size packs he sold were more expensive per millilitre than standard sizes, but it helped try out multiple scents.

# Connected Wacom One Tablet on Laptop running Ubuntu 14.04 LTS
A neighbor reached out to me for technical support. He brought a laptop that runs Ubuntu KITE, which is a derivative of Ubuntu made by an initiative of state government in Kerala. He uses Wacom One tablet for teaching online. It didn’t work out of the box with the Ubuntu version installed in laptop. I fixed this issue earlier by referring resources. I don't remember what I did back then.

I did more reading on the same old resources and came to know that libwacom support is offered by libwacom project to linux based distros. Latest and several previous versions of Ubuntu has out of the box support for wacom tablets. However this laptop which I was working on was an old model and had Ubuntu 14.04, which will go out of its 10 year support quite soon.

The website of libwacom explains very clearly how to get it up and running. I installed the necessary packages. Some non-mandatory components was not successfully installed because of an error possibly rising out of the fact that the ubuntu version is old. I didn't do an upgrade of Ubuntu though because it was not my laptop. Luckily the minimum modules were installed successfully and laptop began taking inputs from the wacom tablet.

The wacom tablet worked seemlessly when I tested it on my laptop. Evidently built in support is present for the device in the recent version of Ubuntu which is running in my laptop. I informed this to my neighbour and also told him that the ubuntu version present in the laptop he brought to me was released in 2014 and its support will end next year.

# Redesigned Personal Web Page
I spend some time to redesign my personal website landing page from scratch to my liking. I assembled a new color palette and created a trademark logo out of my internet nickname. A simple landing page was made in couple of hours. I deployed it through gitlab pages and configured a new domain name different from the name where my current web page is running.
