+++
title = 'January 2024'
date = 2024-02-09T11:35:33+05:30
draft = false
+++

# Tested a Photo Management App with Face Recognision
I uploaded the photos of an event on to my server and used PhotoPrism to extract faces of people. PhotoPrism is a photo management web application that is open source and self hostable. It was a good option considering privacy unlike face recognision capability provided by big companies. I ran PhotoPrism app in the same server where I was running a nextcloud instance so that it is convenient for me to upload photos. I copied photos through nextcloud’s webdav feature by mounting the nextcloud drive to the file explorer in my laptop. After all photos were copied, I scaled my server temporarily to have more resources to aid faster completion of the indexing process of PhotoPrism. Indexing process is expensive. It is when the app goes through all photos to index it based on recognized faces. It was a one time activity. I scaled my server back to normal state after it was complete. Now the app lets me browse through all the unique faces identified from the album and I can click on each face to see the photos in which that face was recognized by the app. There were unrecognized faces and incorrectly recognized faces but the app recognized a good amount of faces overall.