+++
title = 'December 2023'
date = 2024-01-21T16:33:33+05:30
draft = false
+++

# Say Less
Once during a coffee break, a friend called me by a name which I did not know the meaning of. I asked him to tell what it meant. He translated it to an english word but I did not know the meaning of that word either. I insisted him to elaborate, and he said “I told you what it means, now its up to you to look it up!”. I understood It was sufficient for him that he expressed himself and he did not care to elaborate. I used a dictionary later that day.

# How I Wrote and Published this Post
I used outline wiki to draft this post. It is an open source wiki engine that provides a WYSIWYG editor. I hosted the wiki on my server for personal use. I find it useful for frequently editing documents and organizing them. I can edit content via a web browser on my laptop and tablet device. 

This post is not directly published from outline even though it can be done, because I have decided to go with a lightweight solution named hugo for publishing the website in which this content is added. The content of the website is stored in simple markdown format inside a git repository. I had configured gitlab pages to build the website using hugo whenever I push changes to the git repository. I copied the content of this post from my outline instance to a markdown file in my git repository for this website and pushed it as a new commit. The automated build process configured in gitlab updated my website accordingly.
